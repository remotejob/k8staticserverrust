# rustwebserver

## DOCS
https://levelup.gitconnected.com/create-an-optimized-rust-alpine-docker-image-1940db638a6c  
https://github.com/cross-rs/cross/wiki/FAQ#docker-in-docker ?   
https://github.com/FiloSottile/mkcert  

# WORFLOW

rustup update
rustc --version
rustup component add rust-analysis --toolchain 1.64-x86_64-unknown-linux-gnu

cargo init rustwebserver

docker build -t  remotejob/rustwebserver .  
docker push remotejob/rustwebserver

docker run -t -p 8000:8000 remotejob/rustwebserver

docker buildx build -f Dockerfile.builx -t "remotejob/k8staticserverrust:distroless" --platform linux/amd64,linux/arm64 --push .  


