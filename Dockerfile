# -*- mode: dockerfile -*-
# FROM rust:latest AS rust
# RUN rustup target add arm-unknown-linux-musleabihf
# # RUN apt-get update && apt-get -y install binutils-arm-unknown-linux-musleabihf
# RUN apt-get update
# WORKDIR /app
# COPY .cargo ./.cargo
# COPY Cargo.toml Cargo.lock .rustfmt.toml ./
# COPY src ./src
# RUN cargo build --release --target arm-unknown-linux-musleabihf
# RUN ls /app/target

# FROM arm64v8/alpine
# WORKDIR /app
# COPY --from=rust /app/target/arm-unknown-linux-musleabihf/release/rustwebserver ./
# CMD /rustwebserver
# ghcr.io/cross-rs/aarch64-unknown-linux-gnu:0.2.4
#FROM ghcr.io/cross-rs/aarch64-unknown-linux-gnu:latest
FROM rust
RUN dpkg --add-architecture arm64 && \
    apt-get update

RUN  cargo install cross 

RUN ls -a
# RUN which cross
    # apt-get install --assume-yes libfoo:arm64
COPY . .

RUN cross build --target aarch64-unknown-linux-gnu --release
RUN ls target 