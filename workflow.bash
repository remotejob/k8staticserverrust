https://levelup.gitconnected.com/create-an-optimized-rust-alpine-docker-image-1940db638a6c
rustup update
rustc --version
rustup component add rust-analysis --toolchain 1.64-x86_64-unknown-linux-gnu

cargo init rustwebserver

https://github.com/FiloSottile/mkcert local cert

docker build -t  remotejob/rustwebserver .
docker push remotejob/rustwebserver

docker run -t -p 8000:8000 remotejob/rustwebserver

docker buildx build -f Dockerfile.builx -t "remotejob/rustwebserver:scratch" --platform linux/amd64,linux/arm64 --push . && kubectl rollout restart deployment edgecenter-ml-depl -n webs-dev

https://github.com/cross-rs/cross/wiki/FAQ#docker-in-docker ?